import path from "path";
import options from "../lib/options.js";
import vueCreate from "@vue/cli/lib/create.js";
import chalk from "chalk";
import {executeCommand} from "@vue/cli/lib/util/executeCommand.js";
import {ask} from "../lib/questions.js";
import fs from "node:fs/promises";
import {existsSync} from 'node:fs';
import {URL} from "url";
import util from '@vue/cli-shared-utils';
const {hasYarn, hasPnpm3OrLater} = util;

const packageManager = (
    (hasYarn() ? 'yarn' : null) ||
    (hasPnpm3OrLater() ? 'pnpm' : 'npm')
);

const __dirname = (() => {let x = path.dirname(decodeURI(new URL(import.meta.url).pathname)); return path.resolve( (process.platform == "win32") ? x.substr(1) : x ); })();

class componentFactory{
    static async vue3(name, opts, preset) {
        const componentPath = 'x_component_'+name.replace(/\./g, '_');

        let o = (opts || {});
        const p = preset || path.resolve(__dirname, options['vue3 vue-cli']);
        o.preset = p;
        o.skipGetStarted = true;
        await vueCreate(componentPath, o);
        await componentFactory.writeGulpAppFile(componentPath);

        console.log();
        console.log(`👉  `+`${chalk.green('O2OA Comonent "'+componentPath+'" Created!')}`);
        console.log();
        console.log(
            `👉  Get started with the following commands:\n\n` +
            chalk.cyan(` ${chalk.gray('$')} cd ${componentPath}\n`) +
            chalk.cyan(` ${chalk.gray('$')} ${packageManager === 'yarn' ? 'yarn serve' : packageManager === 'pnpm' ? 'pnpm run serve' : 'npm run serve'}`)
        );
    }
    static async vue2(name, opts) {
        componentFactory.vue3(name, opts, path.resolve(__dirname, options.vue2));
    }
    static async vue3_vue_cli(name, opts) {
        componentFactory.vue3(name, opts);
    }

    static async vue3_vite(name, opts) {
        const componentPath = 'x_component_'+name.replace(/\./g, '_');
        const templatePath = path.resolve(__dirname, options["vue3 vite"]);

        if (existsSync(componentPath)){
            console.log();
            console.log(`👉  `+`${chalk.red('Can not Create Component "'+name+'", file already exists "'+componentPath+'" !')}`);

            return '';
        }

        const host = await ask("o2serverHost");
        const port = await ask("o2serverCenterPort");
        const webPort = await ask("o2serverWebPort");
        const isHttps = await ask("isHttps");

        await fs.mkdir(componentPath);
        await componentFactory.cpfile(componentPath, templatePath, {
            projectName: name,
            projectPath: componentPath,
            o2serverHost: host,
            o2serverCenterPort: port,
            o2serverWebPort: webPort,
            isHttps: isHttps
        });

        // if (packageManager==='yarn'){
        //     await executeCommand(packageManager, ['add', '@o2oa/component'], componentPath);
        //     await executeCommand(packageManager, ['add', '@o2oa/oovm'], componentPath);
        //     await executeCommand(packageManager, ['add', '@o2oa/oovm-scripts', '--dve'], componentPath);
        // }else{
        //     await executeCommand(packageManager, ['install', '@o2oa/component', '-save'], componentPath);
        //     await executeCommand(packageManager, ['install', '@o2oa/oovm', '-save'], componentPath);
        //     await executeCommand(packageManager, ['install', '@o2oa/oovm-scripts', '-save-dev'], componentPath);
        // }

        await executeCommand('npm', ['install', '@o2oa/component', '-save'], componentPath);
        await executeCommand('npm', ['install', 'vue@^3.0.0', '-save'], componentPath);
        await executeCommand('npm', ['install', '@vitejs/plugin-vue@^5.0.0', '-save-dev'], componentPath);
        await executeCommand('npm', ['install', 'vite@^5.0.0', '-save-dev'], componentPath);
        await executeCommand('npm', ['install', 'uglify-js', '-save-dev'], componentPath);
        await executeCommand('npm', ['install', 'lodash', '-save-dev'], componentPath);

        await componentFactory.writeGulpAppFile(componentPath);

        console.log();
        console.log(`👉  `+`${chalk.green('O2OA Comonent "'+componentPath+'" Created!')}`);
        console.log();
        console.log(
            `👉  Get started with the following commands:\n\n` +
            chalk.cyan(` ${chalk.gray('$')} cd ${componentPath}\n`) +
            // chalk.cyan(` ${chalk.gray('$')} ${packageManager === 'yarn' ? 'yarn start' : packageManager === 'pnpm' ? 'pnpm run start' : 'npm run start'}`)
            chalk.cyan(` ${chalk.gray('$')} ${'npm run dev'}`)
        );
    }
    static async react_webpack(name, opts) {
        const componentPath = 'x_component_'+name.replace(/\./g, '_');
        const templatePath = path.resolve(__dirname, options["react webpack"]);

        try{
            await executeCommand('npx', ['create-react-app', componentPath, '--scripts-version', '@o2oa/react-scripts']);

            console.log();
            console.log(`👉  `+`Generate O2OA component ... `);
            console.log();

            const pkgStr = await fs.readFile(path.resolve(componentPath, 'package.json'), 'utf8');
            const pkg = JSON.parse(pkgStr);
            pkg.scripts['o2-deploy'] = `cross-env BUILD_PATH=../../dest/${componentPath} PUBLIC_URL=../${componentPath}/ react-scripts build`;
            pkg.scripts['o2-build'] = `cross-env BUILD_PATH=../../../target/o2server/servers/webServer/${componentPath} PUBLIC_URL=../${componentPath}/ react-scripts build`;

            fs.writeFile(path.resolve(componentPath, 'package.json'), JSON.stringify(pkg, '\t'));

            // if (packageManager==='yarn'){
            //     await executeCommand(packageManager, ['add', '@o2oa/component'], componentPath);
            //     await executeCommand(packageManager, ['add', 'cross-env', '--dve'], componentPath);
            // }else{
            await executeCommand('npm', ['install', '@o2oa/component', '-save'], componentPath);
            await executeCommand('npm', ['install', 'cross-env', '-save-dve'], componentPath);
            // }


            console.log();
            console.log(`👉  `+`Configure O2OA development server ... `);
            console.log();

            const host = await ask("o2serverHost");
            const port = await ask("o2serverCenterPort");
            const webPort = await ask("o2serverWebPort");
            const isHttps = await ask("isHttps");

            console.log(host);
            await componentFactory.cpfile(componentPath, templatePath, {
                projectName: name,
                projectPath: componentPath,
                o2serverHost: host,
                o2serverCenterPort: port,
                o2serverWebPort: webPort,
                isHttps: isHttps
            });

            await componentFactory.writeGulpAppFile(componentPath);

            console.log();
            console.log(`👉  `+`${chalk.green('O2OA Comonent "'+componentPath+'" Created!')}`);

        }catch(e){
            throw e;
        }


        // const subprocess = sh.spawn('npm.cmd', ['install'], {
        //     stdio: ['inherit', 'inherit', 'inherit']
        // });
        // subprocess.on('close', (code) => {
        //     console.log(`child process close all stdio with code ${code}`);
        // });
    }
    static async react_vite(name, opts) {
        const componentPath = 'x_component_'+name.replace(/\./g, '_');
        const templatePath = path.resolve(__dirname, options["react vite"]);

        if (existsSync(componentPath)){
            console.log();
            console.log(`👉  `+`${chalk.red('Can not Create Component "'+name+'", file already exists "'+componentPath+'" !')}`);

            return '';
        }

        const host = await ask("o2serverHost");
        const port = await ask("o2serverCenterPort");
        const webPort = await ask("o2serverWebPort");
        const isHttps = await ask("isHttps");

        await fs.mkdir(componentPath);
        await componentFactory.cpfile(componentPath, templatePath, {
            projectName: name,
            projectPath: componentPath,
            o2serverHost: host,
            o2serverCenterPort: port,
            o2serverWebPort: webPort,
            isHttps: isHttps
        });

        // if (packageManager==='yarn'){
        //     await executeCommand(packageManager, ['add', '@o2oa/component'], componentPath);
        //     await executeCommand(packageManager, ['add', '@o2oa/oovm'], componentPath);
        //     await executeCommand(packageManager, ['add', '@o2oa/oovm-scripts', '--dve'], componentPath);
        // }else{
        //     await executeCommand(packageManager, ['install', '@o2oa/component', '-save'], componentPath);
        //     await executeCommand(packageManager, ['install', '@o2oa/oovm', '-save'], componentPath);
        //     await executeCommand(packageManager, ['install', '@o2oa/oovm-scripts', '-save-dev'], componentPath);
        // }

        await executeCommand('npm', ['install', '@o2oa/component', '-save'], componentPath);
        await executeCommand('npm', ['install', 'react@^18.0.0', '-save'], componentPath);
        await executeCommand('npm', ['install', 'react-dom@^18.0.0', '-save'], componentPath);

        await executeCommand('npm', ['install', 'vite@^5.0.0', '-save-dev'], componentPath);
        await executeCommand('npm', ['install', '@vitejs/plugin-react@^4.0.0', '-save-dev'], componentPath);
        await executeCommand('npm', ['install', 'uglify-js', '-save-dev'], componentPath);
        await executeCommand('npm', ['install', 'lodash', '-save-dev'], componentPath);
        await executeCommand('npm', ['install', 'globals@^15.0.0', '-save-dev'], componentPath);
        await executeCommand('npm', ['install', 'axios', '-save-dev'], componentPath);

        await executeCommand('npm', ['install', '@types/react@^18.0.0', '-save-dev'], componentPath);
        await executeCommand('npm', ['install', '@types/react-dom@^18.0.0', '-save-dev'], componentPath);
        await executeCommand('npm', ['install', '@eslint/js@^9.0.0', '-save-dev'], componentPath);
        await executeCommand('npm', ['install', 'eslint@^9.0.0', '-save-dev'], componentPath);
        await executeCommand('npm', ['install', 'eslint-plugin-react@^7.0.0', '-save-dev'], componentPath);
        await executeCommand('npm', ['install', 'eslint-plugin-react-hooks@^5.0.0', '-save-dev'], componentPath);
        await executeCommand('npm', ['install', 'eslint-plugin-react-refresh', '-save-dev'], componentPath); //@^0.0.0

        await componentFactory.writeGulpAppFile(componentPath);

        console.log();
        console.log(`👉  `+`${chalk.green('O2OA Comonent "'+componentPath+'" Created!')}`);
        console.log();
        console.log(
            `👉  Get started with the following commands:\n\n` +
            chalk.cyan(` ${chalk.gray('$')} cd ${componentPath}\n`) +
            // chalk.cyan(` ${chalk.gray('$')} ${packageManager === 'yarn' ? 'yarn start' : packageManager === 'pnpm' ? 'pnpm run start' : 'npm run start'}`)
            chalk.cyan(` ${chalk.gray('$')} ${'npm run dev'}`)
        );
    }
    static async o2_native(name, opts) {
        const componentPath = 'x_component_'+name.replace(/\./g, '_');
        const templatePath = path.resolve(__dirname, options["o2_native"]);

        if (existsSync(componentPath)){
            console.log();
            console.log(`👉  `+`${chalk.red('Can not Create Component "'+name+'", file already exists "'+componentPath+'" !')}`);

            return '';
        }

        await fs.mkdir(componentPath);
        await componentFactory.cpfile(componentPath, templatePath, {
            projectName: name,
            projectPath: componentPath
        });

        await componentFactory.writeGulpAppFile(componentPath, '["move", "min"]');

        console.log();
        console.log(`👉  `+`${chalk.green('O2OA Comonent "'+componentPath+'" Created!')}`);
    }
    static async oovm(name, opts) {
        const componentPath = 'x_component_'+name.replace(/\./g, '_');
        const templatePath = path.resolve(__dirname, options["oovm"]);

        if (existsSync(componentPath)){
            console.log();
            console.log(`👉  `+`${chalk.red('Can not Create Component "'+name+'", file already exists "'+componentPath+'" !')}`);

            return '';
        }

        const host = await ask("o2serverHost");
        const port = await ask("o2serverCenterPort");
        const webPort = await ask("o2serverWebPort");
        const isHttps = await ask("isHttps");

        await fs.mkdir(componentPath);
        await componentFactory.cpfile(componentPath, templatePath, {
            projectName: name,
            projectPath: componentPath,
            o2serverHost: host,
            o2serverCenterPort: port,
            o2serverWebPort: webPort,
            isHttps: isHttps
        });

        // const pkgStr = await fs.readFile(path.resolve(componentPath, 'package.json'), 'utf8');
        // const pkg = JSON.parse(pkgStr);
        // pkg.scripts['o2-deploy'] = `cross-env BUILD_PATH=../../dest/${componentPath} PUBLIC_URL=../${componentPath}/ react-scripts build`;
        // pkg.scripts['o2-build'] = `cross-env BUILD_PATH=../../../target/o2server/servers/webServer/${componentPath} PUBLIC_URL=../${componentPath}/ react-scripts build`;
        //
        // fs.writeFile(path.resolve(componentPath, 'package.json'), JSON.stringify(pkg, '\t'));

        if (packageManager==='yarn'){
            await executeCommand(packageManager, ['add', '@o2oa/component'], componentPath);
            await executeCommand(packageManager, ['add', '@o2oa/oovm'], componentPath);
            await executeCommand(packageManager, ['add', '@o2oa/oovm-scripts', '--dve'], componentPath);
        }else{
            await executeCommand(packageManager, ['install', '@o2oa/component', '-save'], componentPath);
            await executeCommand(packageManager, ['install', '@o2oa/oovm', '-save'], componentPath);
            await executeCommand(packageManager, ['install', '@o2oa/oovm-scripts', '-save-dve'], componentPath);
            // await executeCommand(packageManager, ['install', '', ''], componentPath);
        }

        await componentFactory.writeGulpAppFile(componentPath);

        console.log();
        console.log(`👉  `+`${chalk.green('O2OA Comonent "'+componentPath+'" Created!')}`);
        console.log();
        console.log(
            `👉  Get started with the following commands:\n\n` +
            chalk.cyan(` ${chalk.gray('$')} cd ${componentPath}\n`) +
            chalk.cyan(` ${chalk.gray('$')} ${packageManager === 'yarn' ? 'yarn start' : packageManager === 'pnpm' ? 'pnpm run start' : 'npm run start'}`)
        );
    }
    static async cpfile(cPath, tpPath, opts){
        const files = await fs.readdir(tpPath);
        for (const file of files){
            let p = path.resolve(tpPath, file);
            let stats = await fs.stat(p);
            if (stats.isFile()){
                let content;
                const ext = path.extname(p).toLowerCase();
                if (ext==='.js' || ext==='.jsx' || ext==='.html' || ext==='.css' || ext==='.json'){
                    content = await fs.readFile(p, 'utf8');
                    Object.keys(opts).forEach((k)=>{
                        const reg = new RegExp('\<\%= '+k+' \%\>', 'g');
                        content = content.replace(reg, opts[k]);
                    });

                }else{
                    content = await fs.readFile(p);
                }
                await fs.writeFile(path.resolve(cPath, file), content);
            }
            if (stats.isDirectory()){
                let cp = path.resolve(cPath, file)
                await fs.mkdir(cp, {recursive: true})
                await componentFactory.cpfile(cp, p, opts)
            }
        }
    }
    static async writeGulpAppFile(componentPath, tasks) {
        try {
            const appContent = await fs.readFile('../gulpapps.js', 'utf8');
            const reg = RegExp('\\"folder.*\\"'+componentPath+'\\"');
            if (!reg.test(appContent)){
                const thisComponentText = `{ "folder": "${componentPath}", "tasks": ${tasks || "[]"} }\n`;
                const regexp = RegExp('(var\\s*apps\\s*=\\s*\\[)([\\s]*)({?[\\s\\S]*}?)([\\s\\S]*\\][\\s\\S]*)(module.exports.*)','g');

                const matches = [...[...appContent.matchAll(regexp)][0]];
                matches.shift();
                let updateAppContent = '';
                matches.forEach((match, i)=>{
                    if (i===2){
                        const t = match.trim();
                        updateAppContent += t+((t) ? ',\n    '+thisComponentText : thisComponentText);
                    }else{
                        updateAppContent += match
                    }
                });

                await fs.writeFile('../gulpapps.js', updateAppContent);
            }
        }catch{}
    }
}
export default componentFactory;
