const options = {
    "o2_native": "../template/native",
    "oovm": "../template/oovm",
    "vue3 vue-cli": "../template/vue/vue-cli/preset.json",
    "vue3 vite": "../template/vue/vite",
    "vue2": "../template/vue2/preset.json",
    "react webpack": "../template/react/webpack",
    "react vite": "../template/react/vite"
};
export default options;
